package com.example.lab2

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_registration.*


class RegistrationActivity : AppCompatActivity() {
    companion object {
        const val ARG_EMAIL = "arg_email"
        const val ARG_PASSWORD = "arg_password"

        fun start(context: Context) {
            context.startActivity(Intent(context, RegistrationActivity::class.java))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)
        initUI()
    }

    fun initUI(){
        confirm_button.setOnClickListener {
            var name            = name_string.text.toString()
            var email           = email_string.text.toString()
            var password        = password_string.text.toString()
            var birth_date      = birthDate_string.text.toString()
            var repeat_password = repeat_password_string.text.toString()
            var savedMail       = PreferenceUtils.getEmail(this, email)

            if(!savedMail.equals(email) && password.equals(repeat_password)){
                PreferenceUtils.saveEmail(this, email)
                PreferenceUtils.savePassword(this, email, password)
                PreferenceUtils.saveName(this, name, email)
                LoginActivity.start(this, email, password)
            }
            else{
                email_string.error = "INCORRECT, BOY!"
            }
        }
        back_button.setOnClickListener {
            val intentReg = Intent(this, LoginActivity::class.java)
            this.startActivity(intentReg)
        }
    }


}
