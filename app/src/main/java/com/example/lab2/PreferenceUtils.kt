package com.example.lab2

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager

object PreferenceUtils {

    private const val PREF_EMAIL = "pref_email"
    private const val PREF_PASSWORD = "pref_password"
    private const val PREF_NAME = "pref_name"
    private const val PREF_BIRTH_DATE = "pref_birth_date"

    private fun getPreferences(context: Context)
            = PreferenceManager.getDefaultSharedPreferences(context)

    fun saveEmail(context: Context, email: String) {
        getPreferences(context).edit().putString(PREF_EMAIL + email, email).apply()
    }

    fun getEmail(context: Context, email: String) = getPreferences(context).getString(PREF_EMAIL + email, "")

    fun savePassword(context: Context, email: String, password: String) {
        getPreferences(context).edit().putString("${PREF_PASSWORD}_$email", password).apply()
    }

    fun getPassword(context: Context, email: String)
            = getPreferences(context).getString("${PREF_PASSWORD}_$email", "")

    fun saveName(context: Context, name:String, email: String)
            = getPreferences(context).edit().putString("${PREF_NAME}_$email", name).apply()

    fun getName(context: Context, email: String) = getPreferences(context).getString("${PREF_NAME}_$email", "")

}