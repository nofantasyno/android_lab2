package com.example.lab2

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    companion object {
        const val ARG_EMAIL = "arg_email"
        const val ARG_PASSWORD = "arg_password"
        const val ARG_NAME = "arg_name"

        fun start(context: Context, email:String, password:String, name:String) {
            context.startActivity(
                Intent(
                    context,
                    MainActivity::class.java
                ).apply {
                    putExtra(MainActivity.ARG_EMAIL, email)
                    putExtra(MainActivity.ARG_PASSWORD, password)
                    putExtra(MainActivity.ARG_NAME, name)
                })
        }
    }

    private lateinit var email: String
    private lateinit var password: String
    private lateinit var name:String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        main_text.setText(intent.getStringExtra(ARG_NAME))
    }
}
