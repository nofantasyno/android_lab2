package com.example.lab2

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    companion object {
        const val ARG_EMAIL = "arg_email"
        const val ARG_PASSWORD = "arg_password"

        fun start(context: Context, email: String, password: String) {
            context.startActivity(Intent(context,
                LoginActivity::class.java))
        }
    }

    private lateinit var email: String
    private lateinit var password: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

      //  email     = intent.getStringExtra(ARG_EMAIL)
     //   password  = intent.getStringExtra(ARG_PASSWORD)

        initUI()
    }

    fun initUI() {
        login_button.setOnClickListener {
            val email         = email_view.text.toString()
            val password      = password_view.text.toString()
            val name = PreferenceUtils.getName(this, email)

            val savedEmail    = PreferenceUtils.getEmail(this, email)
            val savedPassword = PreferenceUtils.getPassword(this, savedEmail)

            if(password == savedPassword){
                MainActivity.start(this, email, password, name)
            }
        }
        registration_button.setOnClickListener {
            val intentReg = Intent(this, RegistrationActivity::class.java)
            this.startActivity(intentReg)
        }
    }
}
